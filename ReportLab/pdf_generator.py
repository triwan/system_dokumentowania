# -*- coding: utf-8 -*-

from reportlab.platypus import SimpleDocTemplate, Frame, NextPageTemplate, PageTemplate
from reportlab.platypus import Spacer
from reportlab.lib.units import inch
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.platypus import Table, TableStyle
from reportlab.lib import colors
from reportlab.platypus import PageBreak

import textwrap  # zawijanie tekstu

from ReportLab.slownie import *
from ReportLab.pdf_tools import PDF_Tools

import os
dir_path = os.path.dirname(os.path.realpath(__file__))

#rejestracja czcionek
ttfFileRegular = ('%s%s' % (dir_path, '/font/Verdana.ttf'))
ttfFileBold = ('%s%s' % (dir_path, '/font/Verdanabold.ttf'))
registerFont(TTFont('Verdana',ttfFileRegular))
registerFont(TTFont('Verdanabold',ttfFileBold))


class Faktura(PDF_Tools):
    class Tytul():
        """
        faktura vat nr ...
        """
        @staticmethod
        def styl_1(oryginal, nr_faktury, nr_zamowienia=False, czy_linia_gorna=True, czy_linia_dolna=False):
            """
            Faktura VAT(oryginał) nr: 09/12/2014  -> pogrubione, wieksza czcionka
            wg zamówienia: ssasadaasadsd  -> normalna mniejsza czcionka
            mozna u gory lub/i na dole wyswietlic linie

            oryginal = boolean  -> jesli nie oryginal to kopia
            """
            data = []
            faktura_nr = 'Faktura VAT(%s) nr: %s' % ('oryginał', nr_faktury) if oryginal else 'Faktura VAT (%s) nr: %s' % ('kopia', nr_faktury)

            data.append([faktura_nr])
            data.append(['wg zamówienia: %s' % nr_zamowienia]) if nr_zamowienia is not False else data.append([''])

            table_style=[]
            table_style.append(('FONT', (0,0), (0,0), 'Verdanabold'))  # 1 wiersz
            table_style.append(('FONT', (0,1), (0,1), 'Verdana')) # 2 wiersz
            table_style.append(('FONTSIZE', (0, 0), (0, 0), 10))
            table_style.append(('FONTSIZE', (0, 1), (0, 1), 6))
            table_style.append(('LINEABOVE', (0,0), (-1,0), 0.8, colors.black)) if czy_linia_gorna else None
            table_style.append(('LINEBELOW', (0,0), (-1,0), 0.8, colors.black)) if czy_linia_dolna else None

            # ustawienia szerokosci kolumn , pozycji ...
            table = Table(data, colWidths=[4*inch, 1*inch, 1*inch, 1*inch], hAlign='LEFT')
            table.setStyle(TableStyle(table_style))
            return table

    class Sprzedawca_Nabywca_Dane():
        """
        informacje o sprzedajacym i kupujacym
        """
        @staticmethod
        def styl_1(nabywca_nazwa_firmy, nabywca_adres_ulica, nabywca_adres_miejscowosc, nabywca_nip,
                    sprzedawca_nazwa_firmy, sprzedawca_adres_ulica, sprzedawca_adres_miejscowosc, sprzedawca_nip, sprzedawca_bank_nazwa, sprzedawca_konto_bankowe):
            """
            w tabeli 1 kolumna sprzedawca, 2 kolumna nabywca
            """
            data = []

            data.append(['Sprzedawca', ' ', 'Nabywca'])
            data.append([textwrap.fill(sprzedawca_nazwa_firmy, width=40), ' ', nabywca_nazwa_firmy])
            data.append([sprzedawca_adres_ulica, '' , nabywca_adres_ulica])
            data.append([sprzedawca_adres_miejscowosc, '' , nabywca_adres_miejscowosc])
            data.append([sprzedawca_nip, '' , nabywca_nip])
            data.append([sprzedawca_bank_nazwa, '' , ''])
            data.append([sprzedawca_konto_bankowe, '' , ''])

            table_style=[]
            table_style.append(('FONT', (0,0), (2,0), 'Verdanabold'))  # tytul
            table_style.append(('FONT', (0,1), (-1,-1), 'Verdana'))  # reszta
            table_style.append(('FONT', (0,1), (2,1), 'Verdanabold'))  # nazwy firm
            table_style.append(('VALIGN',(0,0),(-1,-1),'TOP'))  # calosc
            table_style.append(('FONTSIZE', (0, 0), (2, 0), 8))
            table_style.append(('FONTSIZE', (0, 1), (-1, -1), 6))
            table_style.append(('LINEBELOW', (2,0), (2,0), 0.8, colors.black))
            table_style.append(('LINEBELOW', (0,0), (0,0), 0.8, colors.black))

            # ustawienia szerokosci kolumn , pozycji ...
            table = Table(data, colWidths=[2.5*inch, 1*inch, 2.5*inch], rowHeights=[5*mm, 3*mm,3*mm,3*mm,3*mm,3*mm,3*mm], hAlign='LEFT')
            table.setStyle(TableStyle(table_style))
            return table

    class Podpis():
        """
        podpisy nabywcy i wystawcy na samym dole faktury
        """
        @staticmethod
        def styl_1(osoba_wystawiajaca=False):
            """
            poziome linie z napisami - podpis osoby uprawnionej do wystawienia dokumentu, podpis osoby uprawnionej do odbioru dokumentu
            """
            data = []

            data.append(['', ' ', osoba_wystawiajaca]) if osoba_wystawiajaca else False
            data.append(['podpis osoby uprawnionej do odbioru dokumentu', ' ', 'podpis osoby uprawnionej do wystawienia dokumentu'])

            table_style=[]
            table_style.append(('FONT', (0,0), (-1,-1), 'Verdana'))  # reszta
            table_style.append(('FONTSIZE', (0, 0), (-1, -1), 6))

            table_style.append(('LINEBELOW', (2, 0), (2,0), 0.8, colors.black)) if osoba_wystawiajaca else table_style.append(('LINEABOVE', (2, 0), (2,0), 0.8, colors.black))
            table_style.append(('LINEABOVE', (0, 1), (0,1), 0.8, colors.black)) if osoba_wystawiajaca else table_style.append(('LINEABOVE', (0, 0), (0,0), 0.8, colors.black))

            # ustawienia szerokosci kolumn , pozycji ...
            table = Table(data, [2.5*inch, 1*inch, 2.5*inch], hAlign='LEFT')
            table.setStyle(TableStyle(table_style))
            return table

    class Produkty_Uslugi():
        @staticmethod
        def styl_1(dane, podsumowanie):
            """
            dane = [{nazwa:..., ilosc:..., jednostka_miary:..., cena_netto:..., wartosc_netto:... vat:..., cena_brutto:...},]
            podsumowanie = {suma_cena_netto:..., suma_wartosc_netto:..., suma_wartosc_brutto:...}

            tabela z liniami poziomymi i pionowymi, tytul pogrubiony i jasno szary
            """
            data = []

            rowHeights = [9*mm,]  # musze dynamicznie dodawac
            data.append(['Lp.', 'Nazwa', 'Ilość', 'Cena\nnetto (zł)', 'Wartość\nnetto (zł)', 'VAT', 'Wartość\nbrutto (zł)'])  # tytul kolumn

            # dodawanie towarow/uslug
            nr = 1
            for wiersz in dane:
                #data.append([nr, textwrap.fill(wiersz['nazwa'], width=40), ('%s %s') % (wiersz['ilosc'], wiersz['jednostka_miary']), wiersz['cena_netto'], wiersz['wartosc_netto'], wiersz['vat'], wiersz['wartosc_brutto']])  # tytul kolumn
                data.append([nr, ('%s ...') % wiersz['nazwa'][0:60] if len(wiersz['nazwa']) > 60 else wiersz['nazwa'], ('%s %s') % (wiersz['ilosc'], wiersz['jednostka_miary']), wiersz['cena_netto'], wiersz['wartosc_netto'], wiersz['vat'], wiersz['wartosc_brutto']])  # tytul kolumn
                rowHeights.append(4*mm)
                nr+=1

            # dodawanie podsumowania
            rowHeights.append(4*mm)
            data.append(['','','', 'Razem', podsumowanie['suma_wartosc_netto'], '', podsumowanie['suma_wartosc_brutto']])

            table_style=[]
            table_style.append(('FONT', (0,0), (-1,-1), 'Verdana'))  # reszta
            table_style.append(('FONT', (0,0), (-1,0), 'Verdanabold'))  # tytul
            table_style.append(('FONT', (0,-1), (-1,-1), 'Verdanabold'))  # podsumowanie
            table_style.append(('BACKGROUND',(-1,-1),(-1,-1), colors.lightgrey))  # podsumowanie wartosc brutto
            table_style.append(('BACKGROUND',(0,0),(-1,0), colors.lightgrey))
            table_style.append(('FONTSIZE', (0, 0), (-1, -1), 5))
            table_style.append(('INNERGRID', (0,0), (-1,-2), 0.25, colors.grey))
            table_style.append(('BOX', (0,0), (-1,-2), 0.25, colors.black))
            table_style.append(('BOX', (3,-1), (-1,-1), 0.25, colors.black))  # tylko 4 ostatnie komorki w ostatnim wierszu
            table_style.append(('INNERGRID', (3,-1), (-1,-1), 0.25, colors.grey))  # tylko 4 ostatnie komorki w ostatnim wierszu
            table_style.append(('ALIGN',(0,0),(-1,0),'CENTER'))  # tylko tytul
            table_style.append(('VALIGN',(0,0),(-1,0),'MIDDLE'))  # tylko tytul

            table_style.append(('ALIGN',(0,1),(-1,-1),'RIGHT'))  # od wiersz 2 w dol wszystko na prawo
            table_style.append(('VALIGN',(0,1),(-1,-1),'TOP'))  # od wiersz 2 w dol wszystko na prawo
            table_style.append(('ALIGN',(0,1),(0,-1),'CENTER'))  # nr lp. wysrodkowany
            table_style.append(('ALIGN',(1,1),(1,-1),'LEFT'))  # nazwa na lewo

            table = Table(data, colWidths=[0.3*inch, 2.5*inch, 0.7*inch, 0.7*inch, 0.7*inch, 0.4*inch, 0.7*inch], rowHeights=rowHeights, hAlign='LEFT')
            table.setStyle(TableStyle(table_style))
            return table, nr

    class Podsumowanie():
        @staticmethod
        def styl_1(forma_platnosci, termin_platnosci, dostawa, zaplacono, do_zaplaty):
            data = []

            print(slownie(float(do_zaplaty), unit=UNIT_ZLOTY))

            data.append(['Forma płatności :' , forma_platnosci])
            data.append(['Termin płatności :' , termin_platnosci])
            # jesli nie ma dostawy to pusty wiersz
            if len(dostawa) > 0:
                data.append(['Dostawa :' , dostawa])
            else:
                data.append(['' , dostawa])
            data.append(['Zapłacono :' , ('%s zł') % zaplacono])
            data.append(['Do zapłaty :' , ('%s zł') % do_zaplaty])
            data.append(['słownie :' , slownie(float(do_zaplaty), unit=UNIT_ZLOTY)])


            table_style=[]
            table_style.append(('FONT', (0,0), (-1,-1), 'Verdana'))  # wszystko
            table_style.append(('FONT', (0,4), (1,4), 'Verdanabold'))  # do zaplaty + kwota
            table_style.append(('FONTSIZE', (0, 0), (-1, -1), 7))  # wszystko
            table_style.append(('FONTSIZE', (0,4), (1,4), 10))   # do zaplaty + kwota
            table_style.append(('FONTSIZE', (0,5), (1,5), 5))   # do zaplaty + kwota
            table_style.append(('ALIGN',(0,0),(0,-1),'RIGHT'))  # pierwsza kolumna na prawo

            table = Table(data, colWidths=[1.5*inch, 2.5*inch], rowHeights=[4*mm,4*mm,4*mm,4*mm,5*mm, 4*mm], hAlign='RIGHT')
            table.setStyle(TableStyle(table_style))
            return table

    def __init__(self, font='Verdana'):
        self.font = font
        PDF_Tools.__init__(self, font=self.font)


class Faktura_Template(Faktura):
    def __init__(self):
        Faktura.__init__(self)

    def stolargo_template_1(self,
                                nazwa_pliku, miejsce_wystawienia, data_wystawienia, nr_faktury, prefix_nr_faktury, oryginal,
                                nabywca_nazwa_firmy, nabywca_adres_ulica, nabywca_adres_miejscowosc, nabywca_nip,
                                sprzedawca_nazwa_firmy, sprzedawca_adres_ulica, sprzedawca_adres_miejscowosc, sprzedawca_nip, sprzedawca_konto_bankowe, sprzedawca_bank_nazwa,
                                lista_produktow_uslug,
                                podsumowanie_produktow,
                                forma_platnosci, termin_platnosci, dostawa, zaplacono, do_zaplaty,
                                osoba_wystawiajaca_dokument,
                                nr_zamowienia=False
                           ):
        """
        lista produktow - [{'nazwa':'Usługi Informatyczne', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':122200.12, 'vat':'23%', 'cena_brutto':123.23},
        {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23}]

        podsumowanie_produktow - {'suma_ilosc':2, 'suma_cena_netto':111.99, 'suma_wartosc_netto':10234.00, 'suma_wartosc_brutto':123.45}

        nr_zamowienia - jesli false to sie nie wyswietli

        oryginal - True/False j esil nie oryginal to kopia
        """
        # tworze dokument
        doc = SimpleDocTemplate(nazwa_pliku, showBoundary=0,canvasmaker=PDF_Tools.Numer_Strony)

        self.Naglowek = PDF_Tools.Naglowek(
                                            rozmiar_dokumentu=A4, logo=('%s/logo/logo_stolargo_krotkie.png') % dir_path, procentowa_wielkosc_loga = 20,
                                            czy_prawy_opis = True, czy_logo=True, czy_dolna_linia=False, czy_linia_kolo_opisu=True,
                                            opis_prawa_strona = [('Miejsce wystawienia: %s') % miejsce_wystawienia, ('Data wystawienia: %s') % data_wystawienia, 'Nr faktury: %s%s' % (nr_faktury, prefix_nr_faktury)],
                                            odleglosc_naglowka_od_lewej_krawedzi = doc.leftMargin,
                                            grubosc_linii_pionowej_kolo_opisu=.3, grubosc_linii_poziomej_pod_opisem=.1
                                          )

        self.Stopka = PDF_Tools.Stopka(
                                        rozmiar_dokumentu=A4,
                                        opis=u'Stolargo spółka cywilna; NIP: 7343522405; REGON: 122980018, NRB: CCAAAAAAAABBBBBBBBBBBBBBBB (ING Bank Śląski); adres: 33-300 Nowy Sącz, ul. Jagiellońska 74; tel.kont. 734-321-321',
                                        grubosc_linii=.3,
                                        kolor_znakow_hex=0x808080
                                      )

        # odleglosc tekstu od naglowka - tutaj strzelac, nie wiem jak odczyta
        odleglosc_tekstu_od_naglowka = 25

        frameStronaPierwsza = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height, id='pierwsza', topPadding=odleglosc_tekstu_od_naglowka)
        # nastepna - tekst od samej gory
        frameStronaNastepna = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height, id='nastepna')

        # dodawanie templatek dla dokumentu
        # najpierw domyslnie korzysta z pierwszej templatki

        doc.addPageTemplates(
                                [
                                    PageTemplate(id='header',frames=frameStronaPierwsza, onPage=self.Naglowek, onPageEnd=self.Stopka),
                                    PageTemplate(id='footer',frames=frameStronaNastepna, onPageEnd=self.Stopka)
                                ]
                            )

        szerokosc, wysokosc = A4

        elements = []
        elements.append(NextPageTemplate('footer'))  # zaznaczam ze po pierwszej stronie ma byc tylko stopka

        elements.append(Faktura.Tytul.styl_1(oryginal, '%s%s' % (nr_faktury, prefix_nr_faktury), nr_zamowienia, czy_linia_gorna=False))

        elements.append(Spacer(1,0.2*inch))


        elements.append(Faktura.Sprzedawca_Nabywca_Dane.styl_1(nabywca_nazwa_firmy=nabywca_nazwa_firmy, nabywca_adres_ulica=nabywca_adres_ulica, nabywca_adres_miejscowosc=nabywca_adres_miejscowosc,
                                nabywca_nip=nabywca_nip, sprzedawca_nazwa_firmy=sprzedawca_nazwa_firmy, sprzedawca_adres_ulica=sprzedawca_adres_ulica,
                                sprzedawca_adres_miejscowosc=sprzedawca_adres_miejscowosc, sprzedawca_nip=sprzedawca_nip, sprzedawca_konto_bankowe=sprzedawca_konto_bankowe, sprzedawca_bank_nazwa=sprzedawca_bank_nazwa))
        elements.append(Spacer(1,0.2*inch))

        lista_produktow_uslug, ilosc_produktow_uslug = Faktura.Produkty_Uslugi.styl_1(lista_produktow_uslug, podsumowanie_produktow)
        elements.append(lista_produktow_uslug)
        elements.append(Spacer(1,0.2*inch))

        # jesli produktow wiecej niz 15 to nastepny element jakim jest podsumowanie daje na nastepna strone
        if ilosc_produktow_uslug > 25:
            elements.append(PageBreak())
            elements.append(Spacer(1,1*inch))


        elements.append(Faktura.Podsumowanie.styl_1(forma_platnosci=forma_platnosci, termin_platnosci=termin_platnosci, dostawa=dostawa, zaplacono=zaplacono, do_zaplaty=do_zaplaty))
        elements.append(Spacer(1,1*inch))

        elements.append(Faktura.Podpis.styl_1(osoba_wystawiajaca=osoba_wystawiajaca_dokument))

        doc.build(elements, canvasmaker=PDF_Tools.Numer_Strony)
'''
lista_produktow_uslug = [{'nazwa':'Usługi Informatyczne', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':122200.12, 'vat':'23%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},
                            {'nazwa':'dupa', 'ilosc': '1', 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2%', 'cena_brutto':123.23},


                        ]

f = Faktura_Template()
f.stolargo_template_1( nazwa_pliku='basedoc.pdf', miejsce_wystawienia='Nowy Sącz', data_wystawienia='14/07/2017', nr_faktury='44456388678', oryginal=True,
    nabywca_nazwa_firmy='e-grupa', nabywca_adres_ulica='Jamnicka 124', nabywca_adres_miejscowosc='33-987 Kraków', nabywca_nip='734321584',
    sprzedawca_nazwa_firmy='Stolargo', sprzedawca_adres_ulica='Jagiellońska 74', sprzedawca_adres_miejscowosc='33-300 Nowy Sącz', sprzedawca_nip='734564874', sprzedawca_bank_nazwa='ING Bank Śląski', sprzedawca_konto_bankowe='CCAAAAAAAABBBBBBBBBBBBBBBB',
    lista_produktow_uslug = lista_produktow_uslug,
    podsumowanie_produktow = {'suma_ilosc':2, 'suma_cena_netto':111.99, 'suma_wartosc_netto':10234.00, 'suma_wartosc_brutto':123.45},
    forma_platnosci='przelew', termin_platnosci='25/03/2017', dostawa='odbiór osobisty', zaplacono=0, do_zaplaty=465.89,
    osoba_wystawiajaca_dokument='Mateusz Iwański',
    nr_zamowienia=12390908
)
'''
