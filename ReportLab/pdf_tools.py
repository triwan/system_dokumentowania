from reportlab.lib.colors import HexColor
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.pdfbase.pdfmetrics import registerFont

from PIL import Image as Pil_Image

import os
dir_path = os.path.dirname(os.path.realpath(__file__))

#rejestracja czcionek
ttfFileRegular = ('%s%s' % (dir_path, '/font/Verdana.ttf'))
ttfFileBold = ('%s%s' % (dir_path, '/font/Verdanabold.ttf'))
registerFont(TTFont('Verdana',ttfFileRegular))
registerFont(TTFont('Verdanabold',ttfFileBold))


class PDF_Tools(object):
    """
    podstawowe narzedzia
    """

    class Naglowek(object):
        """
        Narzedzia dla naglowka (logo z lewej, opis z prawej, linia pozioma, linia pionowa obok opisu)
        """
        def __init__(self, rozmiar_dokumentu, logo, procentowa_wielkosc_loga, opis_prawa_strona,
                        czy_logo, czy_prawy_opis, czy_linia_kolo_opisu, czy_dolna_linia,
                        odleglosc_naglowka_od_lewej_krawedzi, odleglosc_naglowka_od_prawej_krawedzi=20,
                        grubosc_linii_pionowej_kolo_opisu=.1, grubosc_linii_poziomej_pod_opisem=.1):
            """
            wielkosc_strony (pagesize) = A4, A3 ...
            opis_prawa_strona = ['linia 1', 'linia 2', ...]

            kolejnosc tworzenia elementow jest wazna ze wzgledu na zapisywanie odleglosci!!!
            1 - wstaw_opis_do_naglowka()
            2 - wstaw_logo_do_naglowka()
            3 - wstaw_linie_pionowa_kolo_opisu()
            4 - wstaw_linie_pozioma_pod_naglowkiem()
            """
            self.opis_prawa_strona = opis_prawa_strona

            self.czy_logo = czy_logo
            self.czy_prawy_opis = czy_prawy_opis
            self.czy_linia_kolo_opisu = czy_linia_kolo_opisu
            self.czy_dolna_linia = czy_dolna_linia

            self.grubosc_linii_pionowej_kolo_opisu = grubosc_linii_pionowej_kolo_opisu
            self.grubosc_linii_poziomej_pod_opisem = grubosc_linii_poziomej_pod_opisem

            # ustawienia odleglosci naglowka
            szerokosc, self.wysokosc = rozmiar_dokumentu # podawany w points pt

            self.odleglosc_naglowka_od_gornej_krawedzi = self.wysokosc - 20  # trzeba jeszcze odjac wysokosc elementu, np wysokosc obrazka
            self.odleglosc_naglowka_od_lewej_krawedzi = odleglosc_naglowka_od_lewej_krawedzi
            self.odleglosc_naglowka_od_prawej_krawedzi = szerokosc - odleglosc_naglowka_od_prawej_krawedzi

            self.logo = logo
            self.procentowa_wielkosc_loga = procentowa_wielkosc_loga

        def __call__(self, canvas, doc):
            """
            funkcja wywolywana w momencie dodawania naglowka do BaseDocTemplate
            """
            self.canvas = canvas
            # ustawienia naglowka
            self.wstaw()


        def wstaw_logo_do_naglowka(self):
            procent_wielkosci_obrazu = self.procentowa_wielkosc_loga  # procent rozmiaru wyswietlanego obrazu
            logo = Pil_Image.open(self.logo)  # otwieram obraz poprzez pil aby odczytac jegio wielkosc

            szerokosc_loga = int((float(logo.size[0]) * float(procent_wielkosci_obrazu) / 100))
            wysokosc_loga = int((float(logo.size[1]) * float(procent_wielkosci_obrazu) / 100))

            # zaznaczam odleglosc globalna od glownej krawdzi, informacja potrzebna przy dodawaniu elementow, ktore bede dodawal ponizej
            self.odleglosc_naglowka_od_gornej_krawedzi -= wysokosc_loga

            # dodaje obraz do dokumentu
            self.canvas.drawImage(
                                    self.logo,
                                    x=self.odleglosc_naglowka_od_lewej_krawedzi,
                                    y=self.odleglosc_naglowka_od_gornej_krawedzi,
                                    width=szerokosc_loga, height=wysokosc_loga,
                                    mask='auto'
                                  )

            # informacja dla elementow ktore beda dodawane pod naglowkiem
            if self.odleglosc_naglowka_od_gornej_krawedzi < self.pozycja_koniec_naglowka:
                self.pozycja_koniec_naglowka = self.odleglosc_naglowka_od_gornej_krawedzi

        def wstaw_linie_pozioma_pod_naglowkiem(self):
            self.odleglosc_naglowka_od_gornej_krawedzi -= 10
            #self.canvas.setStrokeColorCMYK(0, 0, 0, .5)
            self.canvas.setLineWidth(self.grubosc_linii_poziomej_pod_opisem)
            self.canvas.line(
                                x1=self.odleglosc_naglowka_od_lewej_krawedzi, y1=self.pozycja_koniec_naglowka-10,
                                x2=self.odleglosc_naglowka_od_prawej_krawedzi, y2=self.pozycja_koniec_naglowka-10
                            )

            # informacja dla elementow ktore beda dodawane pod naglowkiem
            self.pozycja_koniec_naglowka = self.odleglosc_naglowka_od_gornej_krawedzi

        def wstaw_linie_pionowa_kolo_opisu(self):
            odleglosc_od_opisu = 10

            self.canvas.setLineWidth(self.grubosc_linii_pionowej_kolo_opisu)
            #self.canvas.setStrokeColorCMYK(0, 0, 0, .5)
            self.canvas.line(
                                x1=self.opis_odleglosc_zajeta_od_prawej_krawedzi-odleglosc_od_opisu, y1=self.opis_odleglosc_od_gornej_krawedzi,
                                x2=self.opis_odleglosc_zajeta_od_prawej_krawedzi-odleglosc_od_opisu, y2=self.opis_odleglosc_zajeta_od_gornej_krawedzi
                            )

        def wstaw_opis_do_naglowka(self):
            self.canvas.setFont(PDF_Tools.font, 6)
            #  opis po prawej stronie w naglowku
            odleglosc_od_prawej_krawedzi = 170
            odleglosc_od_gornej_krawedzi = 10

            # dla linii pionowej
            # informacja o odleglosci dla elementow, ktore beda dodawane obok opisu (po prawej stronmie)
            # gdzie sie zaczyna od prawej
            self.opis_odleglosc_zajeta_od_prawej_krawedzi = self.odleglosc_naglowka_od_prawej_krawedzi-odleglosc_od_prawej_krawedzi
            # gdzie sie konczy od gory
            # gdzie sie zaczyna od gory
            self.opis_odleglosc_od_gornej_krawedzi = self.odleglosc_naglowka_od_gornej_krawedzi-(odleglosc_od_gornej_krawedzi/2)

            odleglosc_od_krawedzi_gornej = self.odleglosc_naglowka_od_gornej_krawedzi

            for i in self.opis_prawa_strona:
                odleglosc_od_krawedzi_gornej -= odleglosc_od_gornej_krawedzi
                self.canvas.drawString(self.odleglosc_naglowka_od_prawej_krawedzi-odleglosc_od_prawej_krawedzi, odleglosc_od_krawedzi_gornej, i)

            self.canvas.setFont(PDF_Tools.font, 8)

            self.opis_odleglosc_zajeta_od_gornej_krawedzi = odleglosc_od_krawedzi_gornej

            # informacja dla elementow ktore beda dodawane pod naglowkiem
            self.pozycja_koniec_naglowka = odleglosc_od_krawedzi_gornej

        """koniec naglowka"""

        def wstaw(self):
            self.canvas.saveState()
            if self.czy_prawy_opis:
                self.wstaw_opis_do_naglowka()

            if self.czy_logo:
                self.wstaw_logo_do_naglowka()

            if self.czy_linia_kolo_opisu:
                self.wstaw_linie_pionowa_kolo_opisu()

            if self.czy_dolna_linia:
                self.wstaw_linie_pozioma_pod_naglowkiem()

            self.canvas.restoreState()

    class Stopka():
        """
        Narzedzia dla stopki (linia pozioma, opis)
        """

        def __init__(self, rozmiar_dokumentu, opis, grubosc_linii=.1, kolor_znakow_hex = 0x808080):
            """
            wielkosc_strony (pagesize) = A4, A3 ...self.Naglowek

            kolejnosc tworzenia elementow jest wazna ze wzgledu na zapisywanie odleglosci!!!
            1 - wstaw_opis_do_naglowka() lub wstaw_logo_do_naglowka()
            2 - wstaw_linie_pozioma_pod_naglowkiem() lub/i wstaw_linie_pionowa_kolo_opisu()
            """
            self.szerokosc, self.wysokosc = rozmiar_dokumentu
            self.opis = opis
            self.grubosc_linii = grubosc_linii
            self.kolor_znakow_hex = kolor_znakow_hex


        def __call__(self, canvas, doc):
            """
            funkcja wywolywana w momencie dodawania stopki do BaseDocTemplate
            """
            self.canvas = canvas
            # ustawienia naglowka
            self.szerokosc, self.wysokosc = A4

            self.wstaw()

        def __wstaw_linie_pozioma(self):
            odleglosc_od_prawej_lewej_krawedzi = 20
            # informacja dla elementow ktore beda dodawane nad stopka
            self.odleglosc_stopki_od_dolnej_krawedzi = 30
            PDF_Tools.wysokosc_stopki = self.odleglosc_stopki_od_dolnej_krawedzi

            self.canvas.setLineWidth(self.grubosc_linii)
            self.canvas.setStrokeColorCMYK(0, 0, 0, 1)
            self.canvas.line(
                                x1=odleglosc_od_prawej_lewej_krawedzi, y1=self.odleglosc_stopki_od_dolnej_krawedzi,
                                x2=self.szerokosc - odleglosc_od_prawej_lewej_krawedzi, y2=self.odleglosc_stopki_od_dolnej_krawedzi
                            )

        def __wstaw_opis(self):
            self.canvas.setFont(PDF_Tools.font, 5)
            odleglosc_od_linii_poziomej = 10

            #from reportlab.lib.colors import HexColor
            #self.canvas.setFillColor(HexColor(0xd3d3d3))
            self.canvas.drawString(20, self.odleglosc_stopki_od_dolnej_krawedzi-odleglosc_od_linii_poziomej ,self.opis)
            self.canvas.setFont(PDF_Tools.font, 8)

        def wstaw(self):
            self.canvas.saveState()

            self.__wstaw_linie_pozioma()
            self.canvas.setFillColor(HexColor(self.kolor_znakow_hex))
            self.__wstaw_opis()
            self.canvas.setFillColor(HexColor(0x000000))

            self.canvas.restoreState()

        def pozycja_poczatek_stopki(self):
            """zwraca pozycje gdzie rozpoczyna sie Stopka
            nie mozna przekroczyc tej pozycji
            """
            return self.odleglosc_stopki_od_dolnej_krawedzi

    class Numer_Strony(canvas.Canvas):
        """
        http://code.activestate.com/recipes/546511-page-x-of-y-with-reportlab/
        http://code.activestate.com/recipes/576832/
        """
        def __init__(self, *args, **kwargs):
            canvas.Canvas.__init__(self, *args, **kwargs)
            self.pages = []

        def showPage(self):
            """
            On a page break, add information to the list
            """
            self.pages.append(dict(self.__dict__))
            self._startPage()

        def save(self):
            """
            Add the page number to each page (page x of y)'logo_stolargo_dlugie.png'
            """
            page_count = len(self.pages)

            for page in self.pages:
                self.__dict__.update(page)
                self.draw_page_number(page_count)
                canvas.Canvas.showPage(self)

            canvas.Canvas.save(self)

        def draw_page_number(self, page_count):
            """
            Add the page number
            """
            page = "%s/%s" % (self._pageNumber, page_count)
            self.setFont(PDF_Tools.font, 6)
            self.drawRightString(195*mm, PDF_Tools.wysokosc_stopki+5, page)

    def __init__(self, font='Verdana'):
        PDF_Tools.font = font
        self.wysokosc_stopki = 0  # potrzebne przy dodawaniu nr strony
