#from __future__ import print_function
import sys
import os.path
import inspect
import Pyro4

# external
from add_ons.pyro_utils import return_pyro_except_on_failure
from add_ons.logger import Logger

#import os
#dir_path = os.path.dirname(os.path.realpath(__file__))
#sys.path.append(os.path.abspath(dir_path))

from tables_proxy.template_proxy import Template_Proxy

import config as config

#sys.path.append(os.path.abspath('../'))

class Faktura_Proxy(Template_Proxy):
    def __init__(self):
        self.uri = config.uri['Faktura_Proxy']
        Template_Proxy.__init__(self, self.uri)

    @return_pyro_except_on_failure()
    def dodaj_stolargo_template_1(self, towar_usluga, sprzedawca, nabywca, forma_platnosci, osoba_wystawiajaca_dokument, dostawa, faktura):
        callback = self.OBJECT.dodaj_stolargo_template_1(towar_usluga, sprzedawca, nabywca, forma_platnosci, osoba_wystawiajaca_dokument, dostawa, faktura)
        return callback

    @return_pyro_except_on_failure()
    def pokaz_ostatni_nr_faktury_faktury_po_nip_sprzedawca_prefix_nr_faktury(self, nip, prefix_nr_faktury):
        callback = self.OBJECT.pokaz_ostatni_nr_faktury_faktury_po_nip_sprzedawca_prefix_nr_faktury(NIP, prefix_nr_faktury)
        return callback
