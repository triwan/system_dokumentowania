from __future__ import print_function
import sys
import os.path
import inspect
import Pyro4

# external
from add_ons.pyro_utils import return_pyro_except_on_failure
from add_ons.logger import Logger
from tables_proxy.template_proxy import Template_Proxy
import config as config

class Towar_Usluga_Proxy(Template_Proxy):
    def __init__(self):
        self.uri = config.uri['Towar_Usluga_Proxy']
        Template_Proxy.__init__(self, self.uri)
