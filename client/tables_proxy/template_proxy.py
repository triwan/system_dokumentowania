from __future__ import print_function
import sys
import os.path
import inspect
import Pyro4

# external
from add_ons.pyro_utils import return_pyro_except_on_failure
from add_ons.logger import Logger
import config as config

class Template_Proxy(object):
    def __init__(self, uri):
        self.uri = uri
        self.OBJECT = Pyro4.Proxy(self.uri)
        self.logger = Logger()

    @return_pyro_except_on_failure()
    def nazwa_tabeli(self):
        """Zwraca nazwe tabeli
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.nazwa_tabeli()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def ludzka_nazwa(self):
        """Zwraca "ludzka" nazwe
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.ludzka_nazwa()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def pokaz_nazwe_klucza_podstawowego(self):
        """Zwraca nazwe kluca podstawowego
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.pokaz_nazwe_klucza_podstawowego()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def pokaz_ostanie_id(self):
        """Zwraca nazwe kluca podstawowego
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.pokaz_ostanie_id()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def pokaz(self, id):
        """Odczytaj jeden obiekt z bazy SQL uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.pokaz(id)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def pokaz_wszystkie(self):
        """Odczytaj wszystkie obiekty z bazy SQL uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.pokaz_wszystkie()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def dodaj(self, data_tuple):
        """Dodaje element do bazy uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.dodaj(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback


    @return_pyro_except_on_failure()
    def dodaj_bez_commit(self, data_tuple):
        """
        Dodaje element do bazy uzywajac PYRO bez zatwierdzenia zmian
        na koncu zmiany musza zostac zatwierdzone
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.dodaj_bez_commit(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def usun(self, id):
        """Usuwa element z bazy uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (id)', id)
        callback = self.OBJECT.usun(id)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def usun_bez_commit(self, db_table_name, id):
        """Usuwa element z bazy uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (id)', id)
        callback = self.OBJECT.usun_bez_commit(id)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def modyfikuj(self, data_tuple):
        """modyfikuje element z bazy uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.modyfikuj(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def modyfikuj_wiele(self, data_tuple):
        """modyfikuje wiele elementow z bazy uzywajac PYRO
        data_tuple = [
        {'nazwa_firmy':'x', 'opis':'yyyy', 'id_producent':'3'},
        {'nazwa_firmy':'xx', 'opis':'yy1', 'id_producent':'4'},
        ]
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.modyfikuj_wiele(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def dodaj_wiele(self, data_tuple):
        """dodaje wiele elementow z bazy uzywajac PYRO
        data_tuple = [
        {'nazwa_firmy':'x', 'opis':'yyyy', 'id_producent':'3'},
        {'nazwa_firmy':'xx', 'opis':'yy1', 'id_producent':'4'},
        ]
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.dodaj_wiele(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def dodaj_bez_commit_jesli_unikalna_tabela(self,  db_table_name, dane):
        """Dodaje tabele jesli nie istnieje
        zwraca id_tabeli z podanymi danymi
        po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
        funkcja zwraca - {'id_tabela': wartość}
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (db_table_name, dane)', ('%s,%s') % (db_table_name, dane))
        callback = self.OBJECT.dodaj_bez_commit_jesli_unikalna_tabela(db_table_name, dane)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def szukaj(self, data_tuple):
        """Wyszukuje element/y z bazy uzywajac PYRO
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (data_tuple)', data_tuple)
        callback = self.OBJECT.szukaj(data_tuple)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def sprawdz_czy_unikalna_wartosc(self, db_columne_name, dane):
        """Sprawdz czy dana wartosc wystepuje w tabeli
        dane wyjsciowe [{'exists': False or True}]
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (db_columne_name, dane)', ('%s,%s') %(db_columne_name, dane))
        callback = self.OBJECT.sprawdz_czy_unikalna_wartosc(db_columne_name, dane)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback

    @return_pyro_except_on_failure()
    def sprawdz_czy_unikalna_wartosc_przy_modyfikowaniu(self, db_columne_name, dane, id_tabeli_do_modyfikowania):
        """Sprawdz czy dana wartosc wystepuje w tabeli
        nie bierze pod uwage wystepowaniu unikalnosci w tabeli, ktora modyfikujemy
        dane wyjsciowe [{'exists': False or True}]
        """
        self.logger.info(inspect.stack(), 'run')
        self.logger.debug(inspect.stack(), 'function variable (db_columne_name, dane)', ('%s,%s') %(db_columne_name, dane))
        callback = self.OBJECT.sprawdz_czy_unikalna_wartosc_przy_modyfikowaniu(db_columne_name, dane, id_tabeli_do_modyfikowania)
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback


    @return_pyro_except_on_failure()
    def zatwierdz(self):
        """
        Commit - tylko dla funkcji bez commit (dodaj_bez_commit, ...)
        """
        self.logger.info(inspect.stack(), 'run')
        callback = self.OBJECT.zatwierdz()
        self.logger.debug(inspect.stack(), 'callback', callback)
        return callback
