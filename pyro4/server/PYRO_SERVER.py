import sys

if sys.version_info < (3, 0):
    raise("musisz uzyc python-a w wersji 3.x")

import Pyro4
import os.path
import inspect

import logging
#logging.basicConfig()  # or your own sophisticated setup
#logging.getLogger("Pyro4").setLevel(logging.DEBUG)
#logging.getLogger("Pyro4.core").setLevel(logging.DEBUG)

# external
sys.path.append(os.path.abspath('../'))
import config

from tables.Dostawa import DOSTAWA
from tables.Faktura_Towar_Usluga import FAKTURA_TOWAR_USLUGA
from tables.Faktura import FAKTURA
from tables.Forma_Platnosci import FORMA_PLATNOSCI
from tables.Jednostka_Miary import JEDNOSTKA_MIARY
from tables.Klient_Dzialalnosc_Gospodarcza import KLIENT_DZIALALNOSC_GOSPODARCZA
from tables.Nabywca import NABYWCA
from tables.Osoba_Wystawiajaca_Dokument import OSOBA_WYSTAWIAJACA_DOKUMENT
from tables.Pkwiu import PKWIU
from tables.Sprzedawca import SPRZEDAWCA
from tables.Towar_Usluga import TOWAR_USLUGA
from tables.Vat import VAT
from tables.Serwer_Plikowy import SERWER_PLIKOWY
from tables.Templatka import TEMPLATKA

def main():
    #netstat -antu   - sprawdz wolne porty
    Pyro4.config.DETAILED_TRACEBACK=1  # detale wyjatkow
    Pyro4.config.HOST = config.pyro['host']
    Pyro4.config.THREADPOOL_SIZE = 300
    Pyro4.Daemon.serveSimple(
            {
                DOSTAWA: ("%s.DOSTAWA" % config.pyro['name']),
                FAKTURA_TOWAR_USLUGA: ("%s.FAKTURA_TOWAR_USLUGA" % config.pyro['name']),
                FAKTURA: ("%s.FAKTURA" % config.pyro['name']),
                FORMA_PLATNOSCI: ("%s.FORMA_PLATNOSCI" % config.pyro['name']),
                JEDNOSTKA_MIARY: ("%s.JEDNOSTKA_MIARY" % config.pyro['name']),
                KLIENT_DZIALALNOSC_GOSPODARCZA: ("%s.KLIENT_DZIALALNOSC_GOSPODARCZA" % config.pyro['name']),
                NABYWCA: ("%s.NABYWCA" % config.pyro['name']),
                OSOBA_WYSTAWIAJACA_DOKUMENT: ("%s.OSOBA_WYSTAWIAJACA_DOKUMENT" % config.pyro['name']),
                PKWIU: ("%s.PKWIU" % config.pyro['name']),
                SPRZEDAWCA: ("%s.SPRZEDAWCA" % config.pyro['name']),
                TOWAR_USLUGA: ("%s.TOWAR_USLUGA" % config.pyro['name']),
                VAT: ("%s.VAT" % config.pyro['name']),
                SERWER_PLIKOWY: ("%s.SERWER_PLIKOWY" % config.pyro['name']),
                TEMPLATKA: ("%s.TEMPLATKA" % config.pyro['name']),
            },
            ns=False, port=config.pyro['port'])

if __name__=="__main__":
    main()
