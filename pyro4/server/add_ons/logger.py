import logging
import logging.handlers

class Logger(object):
    """Logowanie do syslog
    Logowanie do /var/log/syslog

    użycie:
    import inspect  # file_name + function_name
    logger = Logger()
    logger.info(inspect.stack(), your_info)
    logger.debug(inspect.stack(), your_variable, your_records)
    logger.warning(inspect.stack(), your_warning)
    logger.error(inspect.stack(), your_error)
    """
    def __init__(self):
        self.name = 'Python: Pyro_Server'
        self.logger = logging.getLogger(self.name)
        self.formatter = '%(name)s - %(levelname)s - %(message)s'
        logging.basicConfig(level=logging.DEBUG)
        self.handler = logging.handlers.SysLogHandler(address='/dev/log')  # log to /var/log/syslog
        self.formatter = logging.Formatter(self.formatter)
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

    def info(self, inspect, infoData):
        #self.logger.info(('file=%s%sfunction:%s%s, message=%s' % (inspect[0][1],'-->',inspect[0][3],'()', infoData)))
        return

    def debug(self, inspect, variable_name, debugData):
        self.logger.debug(('file=%s%sfunction:%s%s, %s=%s' % (inspect[0][1],'-->',inspect[0][3],'()', variable_name, debugData)))
        return

    def warning(self, inspect, warningData):
        self.logger.warning(('file=%s%sfunction:%s%s, message=%s' % (inspect[0][1],'-->',inspect[0][3],'()', warningData)))
        return

    def error(self, inspect, errorData):
        self.logger.error(('file=%s%sfunction:%s%s, exception=%s' % (inspect[0][1],'-->',inspect[0][3],'()', errorData)))
        return
