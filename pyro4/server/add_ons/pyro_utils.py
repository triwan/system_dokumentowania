import Pyro4
import Pyro4.util
import inspect

#add-ons
from add_ons.logger import Logger

def return_pyro_except_on_failure():
  def decorate(f):
    def applicator(*args, **kwargs):
      try:
         return f(*args,**kwargs)
      except Exception:
         logger = Logger()
         logger.error(inspect.stack(), "Pyro traceback:")
         logger.error(inspect.stack(), ('\n',"".join(Pyro4.util.getPyroTraceback())))
         print("Pyro traceback:")
         print("".join(Pyro4.util.getPyroTraceback()))
    return applicator
  return decorate
