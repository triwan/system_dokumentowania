import inspect
import sys
import os.path

# external
sys.path.append(os.path.abspath('../'))
import config
from database_tools.database_connection import Database
from add_ons.logger import Logger


class DB_aggregator(object):
    # week references - garbage collector
    from weakref import WeakValueDictionary
    _instances = WeakValueDictionary()
    @property
    def Count(self):
        return len(self._instances)
    #--

    def __init__(self):
        self.logger = Logger()
        self.db = Database(host=config.database['host'], name=config.database['name'],
                            user=config.database['user'], password=config.database['password'])

    def reconnect(self):
        """Restartuj polaczenie z baza
        """
        self.logger.error(inspect.stack(), 'Ponownie probuje polaczyc sie z baza SQL')
        self.db.reconnect()

    def prepare_dict_seperate_item_values(self, data : dict):
        """Przygotuj slownik do zapytania
        usuwam wartosci, zostawiam klucze
        data = {'nazwa_firmy':'x', 'opis':'yyyy', 'id_producent':'3'
        callback = string ('nazwa_firmy', 'id_producent', 'opis')
        """
        '''
        if isinstance(data, dict):  # sprawdzam czy dict
            if len(data.keys()) == 1:  # jesli mamy jeden element w liscie zwraca ('jakies dane',) a musi być ('jakies_dane')
                items = str(tuple(data.keys())).replace(',','')  # usuwam wszystkie znaczniki \' - nazwy kolumn oraz przecinek w jednym elem.
            else:
                items = str(tuple(data.keys()))  # usuwam wszystkie znaczniki \'

            callback = {'items':items}
            return callback
        '''
        if isinstance(data, dict):  # sprawdzam czy dict
            items = str(tuple(data.keys()))  # usuwam wszystkie znaczniki \'
            callback = {'items':items}
            return callback

    def create_execute_many_statement_to_update_from_list_with_tuple(self, data_search_tuple, column_id_name):
        """dokłada do zapytania executemany dla update
        data_search_tuple = tuple ('id_producent', 'nazwa_firmy', 'opis')
        column_id_name = 'id_producent'
        callback = opis=%(opis)s,nazwa_firmy=%(nazwa_firmy)s
        ... nazwa_firmy=%(nazwa_firmy)s,opis=%(opis)s where ...
        """
        # where key like '%teu%' and nip like '%34%'
        new_tuple = []
        for s in list(data_search_tuple):
            if not s == column_id_name:
                new_tuple.append(s)

        without_table_id = tuple(new_tuple)

        where_add = None
        for key in without_table_id:
            if where_add == None :
                where_add = ('%s=%s(%s)s') % (key, '%', key)
            else:
                where_add += (',%s=%s(%s)s') % (key, '%', key)
        callback = ('%s') % (where_add)
        return callback

    def create_execute_many_statement_to_insert_from_list_with_tuple(self, data_search_tuple, column_id_name):
        """dokłada do zapytania executemany dla update
        data_search_tuple = tuple ('id_producent', 'nazwa_firmy', 'opis')
        column_id_name = 'id_producent'
        callback = opis=%(opis)s,nazwa_firmy=%(nazwa_firmy)s
        ... nazwa_firmy=%(nazwa_firmy)s,opis=%(opis)s where ...
        """
        # where key like '%teu%' and nip like '%34%'
        new_tuple = []
        for s in list(data_search_tuple):
            if not s == column_id_name:
                new_tuple.append(s)

        without_table_id = tuple(new_tuple)

        where_add = None
        for key in without_table_id:
            if where_add == None :
                where_add = ('%s(%s)s') % ('%', key)
            else:
                where_add += (', %s(%s)s') % ('%', key)
        callback = ('%s') % (where_add)
        return callback

    def prepare_dict_serperate_item_values(self, data : dict):
        """Przygotuj slownik dla zapytania
        {'a':1, 'b':2} = {items:(a,b) , values:(1, 2)}
        ex. insert into table callback['items'] values callback['values']
        """
        if isinstance(data, dict):  # sprawdzam czy dict
            self.logger.info(inspect.stack(), 'run')
            self.logger.debug(inspect.stack(), 'function variable (data)', data)

            if len(data.keys()) == 1:  # jesli mamy jeden element w liscie zwraca ('jakies dane',) a musi być ('jakies_dane')
                items = str(tuple(data.keys())).replace("\'","").replace(',','')  # usuwam wszystkie znaczniki \' - nazwy kolumn oraz przecinek w jednym elem.
            else:
                items = str(tuple(data.keys())).replace("\'","")  # usuwam wszystkie znaczniki \'
            if len(data.values()) == 1:  # jesli mamy jeden element w liscie zwraca ('jakies dane',) a musi być ('jakies_dane')
                values = str(tuple(data.values())).replace(',','')
            else:
                values = str(tuple(data.values()))

            callback = {'items':items, 'values':values}
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        else:
            self.logger.error(inspect.stack(), ('nieprawidlowy typ danych...wymagane dict, przekazane %s=%s' % (data, type(data))))


    def prepare_dict_item_equal_to_values(self, table_name, data : dict, separator):
        """Przygotuj slownik dla zapytania
        jesli jest id w data to przekazuje
        separator = (,) (AND) (OR)
        {'a':1, 'b':2} = string item(separator)value, ...
        ex. update table set items='values', ...
        ex. select table where items='values' AND ...
        ex. select table where items='values' OR ...
        """
        if isinstance(data, dict):  # sprawdzam czy dict
            self.logger.info(inspect.stack(), 'run')
            self.logger.debug(inspect.stack(), 'function variable (data)', data)
            table_id = None
            try:
                table_id = data[('id_%s')%table_name]  # czytam table_id
                data.pop('id_%s' % table_name)  # usuwam table_id
            except Exception as e:
                #self.logger.error(inspect.stack(), ('nieprawidlowy typ danych...wymagane id_tabeli w dict, przekazane %s=%s - %s' % (data, type(data), e)))
                pass

            callback = ''
            for i,v in data.items():
              if v != None:
                  callback+=(('%s = \'%s\' %s ')%(i,v,separator))  # tworze string
              else:
                  callback+=(('%s = NULL %s ')%(i,separator))  # tworze string, None nie moze byc w ''
            callback = callback[:-(len(separator)+1)]  # usuwam ostatnie znaki po dlugosci separator (,=-1 and=-3), +1 to pusty znak dodawany na koncu separatora
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback, table_id
        else:
            self.logger.error(inspect.stack(), ('nieprawidlowy typ danych...wymagane dict, przekazane %s=%s' % (data, type(data))))

    def execute_command(self, command):
        try:
            self.logger.info(inspect.stack(), 'run')
            self.logger.debug(inspect.stack(), 'function variable (command)', command)
            callback = self.db.execute_command(command)
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()


    def execute_many(self, query, tuple_of_dict):
        try:
            self.logger.info(inspect.stack(), 'run')
            self.logger.debug(inspect.stack(), 'function variable (query)', query)
            self.logger.debug(inspect.stack(), 'function variable (tuple_of_dict)', tuple_of_dict)
            callback = self.db.execute_many(query, tuple_of_dict)
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()

    def fetch_one(self):
        try:
            self.logger.info(inspect.stack(), 'run')
            callback = self.db.fetch_one()
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()

    def fetch_many(self, how_many):
        try:
            self.logger.info(inspect.stack(), 'run')
            self.logger.debug(inspect.stack(), 'function variable (how_many)', how_many)
            callback = self.db.fetch_many(how_many)
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()

    def fetch_all(self):
        try:
            self.logger.info(inspect.stack(), 'run')
            callback = self.db.fetch_all()
            self.logger.debug(inspect.stack(), 'callback', callback)
            return callback
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()

    def make_changes(self):
        try:
            self.logger.info(inspect.stack(), 'run')
            return self.db.make_changes()
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()

    def discard_changes(self):
        try:
            self.logger.info(inspect.stack(), 'run')
            return self.db.rollback()
        except Exception as e:
            self.logger.error(inspect.stack(), e)
            self.reconnect()


    # koncze polaczenie z baza podczas usuwaniu obiektu
    def __del__(self):
        try:
            self.logger.info(inspect.stack(), 'run')
            self.db.close_communication()
        except:
            pass
        if self.Count == 0:
            self.logger.info(inspect.stack(), 'Last database_aggregator object deleted')
        else:
            self.logger.info(inspect.stack(), 'Counter database_aggregator remaining')
