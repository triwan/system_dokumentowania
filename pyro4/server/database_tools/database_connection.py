import psycopg2
import psycopg2.extras
import time

class Database(object):
    def __init__(self, host, name, user, password):
        self.DBHOST = host
        self.DBNAME = name
        self.DBUSER = user
        self.DBPASSWORD = password
        self.DBCONNECTION = None
        self.CURSOR = None

        self.connect()  # nawiaz polaczenie
          # sprawdzaj czy jest polaczonie z baza

    def connect(self):
        if self.DBCONNECTION is None:
            self.DBCONNECTION = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'"
                                                % (self.DBNAME, self.DBUSER, self.DBHOST, self.DBPASSWORD))
            self.CURSOR = self.DBCONNECTION.cursor(cursor_factory=psycopg2.extras.RealDictCursor)  # ustawiam kursor zeby wracal slownik
        else:
            self.close_communication()
            raise Exception("Instancja posiada juz aktywne polaczenie!!!!", e)

    def reconnect(self):
        """
        Restartuj polaczenie z baza
        wlacza sie dopiero po 2 zdalnym wywolaniu funkcji
        popatrzec jeszcze na to
        """
        try:
            while(self.DBCONNECTION.get_transaction_status() != 0):  # jesli polaczenie zostalo zerwane, wystapi wyjatek o braku funkcji w obiekcie
                time.sleep(.500)
                if self.DBCONNECTION.get_transaction_status() != 0:  # jesli nie ma polaczenia - 4, 0 - jest polaczenie
                    try:
                        self.close_communication()
                    except Exception as e:
                        pass
                    self.DBCONNECTION = None
                    self.connect()
        except AttributeError as e:
            self.DBCONNECTION = None
            self.connect()
            pass

    def execute_command(self, command):
        try:
            self.CURSOR.execute(command)
        except psycopg2.Error as e:
            self.close_communication()
            raise Exception('Zamykam polaczenie z baza. Blad w wykonywaniu zapytania SQL (execute_command) - ', e)

    def execute_many(self, query, tuple_of_dict):
        """
        The cur.executemany statement will automatically iterate through the dictionary and execute the INSERT query for each row.
        tuple_of_dict = ({"first_name":"Joshua", "last_name":"Drake"},
                         {"first_name":"Steven", "last_name":"Foo"},
                         {"first_name":"David", "last_name":"Bar"})
        .executemany(\"\"\"INSERT INTO bar(first_name,last_name) VALUES (%(first_name)s, %(last_name)s)\"\"\", tuple_of_dict)
        """
        try:
            self.CURSOR.executemany(query, tuple_of_dict)
        except psycopg2.Error as e:
            self.close_communication()
            raise Exception('Zamykam polaczenie z baza. Blad w wykonywaniu zapytania SQL (execute_command) - ', e)

    def fetch_one(self):
        """
        Fetch the next row of a query result set, returning a single tuple, or None when no more data is available:
        """
        try:
            return self.CURSOR.fetchone()
        except psycopg2.Error as e:
            self.close_communication()
            raise Exception('Zamykam polaczenie z baza. Blad przy zwracaniu informacji z fetch_one', e)

    def fetch_many(self, how_many):
        """
        Fetch the next set of rows of a query result, returning a list of tuples. An empty list is returned when no more rows are available.
        """
        try:
            return self.CURSOR.fetchmany(how_many)
        except psycopg2.Error as e:
            self.close_communication()
            raise Exception('Zamykam polaczenie z baza. Blad przy zwracaniu informacji z fetch_many', e)

    def fetch_all(self):
        """
        Fetch all (remaining) rows of a query result, returning them as a list of tuples. An empty list is returned if there is no more record to fetch.
        """
        try:
            return self.CURSOR.fetchall()
        except psycopg2.Error as e:
            self.close_communication()
            raise Exception('Zamykam polaczenie z baza. Blad przy zwracaniu informacji z fetch_all', e)

    def make_changes(self):
        """
        Commit any pending transaction to the database.
        By default, Psycopg opens a transaction before executing the first command: if commit() is not called, the effect of any data manipulation will be lost.
        """
        if self.DBCONNECTION is not None:
            self.DBCONNECTION.commit()
        else:
            self.close_communication()
            raise Exception("Nie ma nawiazanego polaczenia z baza")

    def rollback(self):
        """
        rollback - jesli wywolamy przed commit, wszystkie zmiany zostana odrzucone
        """
        if self.DBCONNECTION is not None:
            self.DBCONNECTION.rollback()
        else:
            self.close_communication()
            raise Exception("Nie ma nawiazanego polaczenia z baza")

    def close_communication(self):
        self.CURSOR.close()
        self.DBCONNECTION.close()
