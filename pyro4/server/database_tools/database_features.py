import sys
import os.path

#external
sys.path.append(os.path.abspath('../'))

def create_where_statement_from_dict_with_tuple(query, data_search_dict):
    """dokłada do zapytania klauzule where/and like
    wywolanie:
    query = str - zapytanie poczatkowe select ...
    data_search_dict = [{},..] - w slowniku nazwa tabeli i wartosc
    """
    # where key like '%teu%' and nip like '%34%'
    where_add = None
    for key, value in data_search_dict.items():
        if where_add == None :
            where_add = (' where %s like \'%s%s%s\'') % (key, str('%'), value, str('%'))
        else:
            where_add += (' and %s like \'%s%s%s\'') % (key, str('%'), value, str('%'))
    callback = ('%s %s') % (query, where_add)
    return callback
