import sys
import os.path

#external
sys.path.append(os.path.abspath('../'))
from database_tools.database_aggregator import DB_aggregator

DB = DB_aggregator()

def pokaz(db_table_name, id):
    """Odczytaj jeden obiekt z bazy
    """
    DB.execute_command(('SELECT * FROM %s WHERE id_%s = %s' % (db_table_name, db_table_name, id)))
    callback = DB.fetch_all()
    return callback

def pokaz_wszystkie(db_table_name):
    """Odczytaj wszystkie obiekty z bazy
    """
    DB.execute_command('SELECT * FROM %s' % db_table_name)
    callback = DB.fetch_all()
    return callback

def pokaz_ostanie_id(db_table_name):
    """Zwraca ostanie id tabeli
    zwraca {'max':...}
    """
    query = 'select max(id_%s) from %s' % (db_table_name, db_table_name)
    callback = DB.fetch_all(query)
    return callback

def usun(db_table_name, id):
    """Usuwa element z bazy
    """
    query = 'DELETE FROM %s WHERE id_%s = %s' % (db_table_name, db_table_name, id)
    DB.execute_command(query)
    DB.make_changes()

def usun_bez_commit(db_table_name, id):
    """Usuwa element z bazy
    """
    query = 'DELETE FROM %s WHERE id_%s = %s' % (db_table_name, db_table_name, id)
    DB.execute_command(query)

def dodaj_bez_commit(db_table_name, dane):
    """Dodaje element do bazy bez commit
    po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
    dane wejsciowe = slownik {'nazwa kolumny':'dane',...}
    zwraca id dodanego elementu - {'id_dzialalnosc_gospodarcza': ...}
    """
    data = DB.prepare_dict_serperate_item_values(dane)
    query = 'INSERT INTO %s %s VALUES %s RETURNING id_%s' % (db_table_name, data['items'], data['values'], db_table_name)
    DB.execute_command(query)
    callback = DB.fetch_one()
    return callback

def dodaj_bez_commit_bez_zwrotki(db_table_name, dane):
    """Dodaje element do bazy bez commit
    po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
    dane wejsciowe = slownik {'nazwa kolumny':'dane',...}
    """
    data = DB.prepare_dict_serperate_item_values(dane)
    query = 'INSERT INTO %s %s VALUES %s' % (db_table_name, data['items'], data['values'])
    DB.execute_command(query)

def dodaj_bez_zwrotki(db_table_name, dane):
    """Dodaje element do bazy
    dane wejsciowe = slownik {'nazwa kolumny':'dane',...}
    """
    data = DB.prepare_dict_serperate_item_values(dane)
    query = 'INSERT INTO %s %s VALUES %s' % (db_table_name, data['items'], data['values'])
    DB.execute_command(query)
    DB.make_changes()

def dodaj(db_table_name, dane):
    """Dodaje element do bazy
    dane wejsciowe = slownik {'nazwa kolumny':'dane',...}
    zwraca id dodanego elementu - {'id_dzialalnosc_gospodarcza': ...}
    """
    data = DB.prepare_dict_serperate_item_values(dane)
    query = 'INSERT INTO %s %s VALUES %s RETURNING id_%s' % (db_table_name, data['items'], data['values'], db_table_name)
    DB.execute_command(query)
    DB.make_changes()
    callback = DB.fetch_one()
    return callback

def modyfikuj(db_table_name, dane):
    """Modyfikuje elementy w bazie
    dane wejsciowe (musi być id tabeli!) = slownik {'nazwa kolumny':'dane',...}
    zwraca id zmodyfikowanego elementu - {'id_dzialalnosc_gospodarcza': ...}
    """
    data = DB.prepare_dict_item_equal_to_values(db_table_name, dane, ',')
    query = 'UPDATE %s SET %s WHERE id_%s = %s RETURNING id_%s' % (db_table_name, data[0], db_table_name,  data[1], db_table_name)
    DB.execute_command(query)
    DB.make_changes()
    callback = DB.fetch_one()
    return callback

def modyfikuj_bez_commit(db_table_name, dane):
    """Modyfikuje elementy w bazie
    po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
    dane wejsciowe (musi być id tabeli!) = slownik {'nazwa kolumny':'dane',...}
    zwraca id zmodyfikowanego elementu - {'id_dzialalnosc_gospodarcza': ...}
    """
    data = DB.prepare_dict_item_equal_to_values(db_table_name, dane, ',')
    query = 'UPDATE %s SET %s WHERE id_%s = %s RETURNING id_%s' % (db_table_name, data[0], db_table_name,  data[1], db_table_name)
    DB.execute_command(query)
    callback = DB.fetch_one()
    return callback

def dodaj_wiele(db_table_name, list_of_dict, nazwa_id_klucza):
    """Dodaje wiele elementow na raz
    db_table_name = nazwa tabeli
    list_of_dict = [
    {'nazwa_firmy':'x', 'opis':'yyyy', 'id_producent':'3'},
    {'nazwa_firmy':'xx', 'opis':'yy1', 'id_producent':'4'},
    ]
    nazwa_id_klucza = nazwa klucza podstawowego
    INSERT INTO %s (first_name,last_name) VALUES (%(first_name)s, %(last_name)s))
    """
    data = DB.prepare_dict_serperate_item_values(list_of_dict[0])
    str_items = DB.prepare_dict_seperate_item_values(list_of_dict[0])['items']  # callback = string ('nazwa_firmy', 'id_producent', 'opis')
    tuple_items = eval(str_items)  # callback = tuple ('nazwa_firmy', 'id_producent', 'opis')
    after_values = DB.create_execute_many_statement_to_insert_from_list_with_tuple(tuple_items, nazwa_id_klucza)  # callback = opis=%(opis)s,nazwa_firmy=%(nazwa_firmy)s
    query = ('INSERT INTO %s %s VALUES (%s)' % (db_table_name, data['items'], after_values))
    DB.execute_many(query, tuple(list_of_dict))
    DB.make_changes()

def modyfikuj_wiele(db_table_name, list_of_dict, nazwa_id_klucza):
    """Modyfikuje wiele elementow na raz
    db_table_name = nazwa tabeli
    list_of_dict = [
    {'nazwa_firmy':'x', 'opis':'yyyy', 'id_producent':'3'},
    {'nazwa_firmy':'xx', 'opis':'yy1', 'id_producent':'4'},
    ]
    nazwa_id_klucza = nazwa klucza podstawowego
    update producent set nazwa_firmy=%(nazwa_firmy)s,opis=%(opis)s where id_producent=%(id_producent)s
    """
    str_items = DB.prepare_dict_seperate_item_values(list_of_dict[0])['items']  # callback = string ('nazwa_firmy', 'id_producent', 'opis')
    tuple_items = eval(str_items)  # callback = tuple ('nazwa_firmy', 'id_producent', 'opis')
    after_set = DB.create_execute_many_statement_to_update_from_list_with_tuple(tuple_items, nazwa_id_klucza)  # callback = opis=%(opis)s,nazwa_firmy=%(nazwa_firmy)s
    query = ('update %s set %s where %s=%s(%s)s' % (db_table_name, after_set, nazwa_id_klucza, '%', nazwa_id_klucza))  # update producent set nazwa_firmy=%(nazwa_firmy)s,opis=%(opis)s where id_producent=%(id_producent)s
    #cur.executemany("""INSERT INTO %s (first_name,last_name) VALUES (%(first_name)s, %(last_name)s) % (db_table_name, data['items'])""", namedict)
    DB.execute_many(query, tuple(list_of_dict))
    DB.make_changes()

def szukaj(db_table_name, dane):
    """Szukaj elementy w bazie
    dane wejsciowe = slownik {'nazwa kolumny':'dane',...}sprawdz_czy_unikalna_wartosc
    """
    data = DB.prepare_dict_item_equal_to_values(db_table_name, dane, 'AND')
    query = 'SELECT * FROM %s WHERE %s' % (db_table_name, data[0])
    DB.execute_command(query)
    DB.make_changes()
    callback = DB.fetch_one()
    return callback

def sprawdz_czy_unikalna_wartosc(db_table_name, db_columne_name, dane):
    """Sprawdz czy dana wartosc wystepuje w tabeli
    dane wyjsciowe [{'exists': False or True}]
    """
    DB.execute_command(('select exists(select 1 from %s where %s = \'%s\')' % (db_table_name, db_columne_name, dane)))
    callback = DB.fetch_all()
    return callback

def sprawdz_czy_unikalna_wartosc_przy_modyfikowaniu(db_table_name, db_columne_name, dane, id_tabeli_do_modyfikowania):
    """Sprawdz czy dana wartosc wystepuje w tabeli
    nie bierze pod uwage wystepowaniu unikalnosci w tabeli, ktora modyfikujemy
    dane wyjsciowe [{'exists': False or True}]make_changes
    """
    DB.execute_command(('select exists(select 1 from %s where %s = \'%s\' and id_%s != %s)' % (db_table_name, db_columne_name, dane, db_table_name, id_tabeli_do_modyfikowania)))
    callback = DB.fetch_all()
    return callback

def dodaj_bez_commit_jesli_unikalna_tabela(db_table_name, dane):
    """Dodaje tabele jesli nie istnieje
    zwraca id_tabeli z podanymi danymi
    po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
    funkcja zwraca - {'id_tabela': wartość}
    """
    data = DB.prepare_dict_serperate_item_values(dane)
    select_data = DB.prepare_dict_item_equal_to_values(db_table_name, dane, 'AND')
    query = '''INSERT INTO %s %s select %s where not exists
                (select id_%s from %s where %s)''' % (
                db_table_name, data['items'], data['values'].replace('(','').replace(')',''), db_table_name, db_table_name, select_data[0])
    DB.execute_command(query)  # jesli nie istnieja podane dane to wstawiam, jesli dane istnieja to nic nie rob
    data = DB.prepare_dict_item_equal_to_values(db_table_name, dane, 'AND')
    query = 'SELECT id_%s FROM %s WHERE %s' % (db_table_name, db_table_name, data[0])
    DB.execute_command(query)  # szukam id_tabela z takimi danymi
    callback = DB.fetch_one()  # tabela
    return callback

def wywolaj_z_reki(zapytanie):
    """Wywolanie zapytania z reki
    """
    DB.execute_command(zapytanie)
    callback = DB.fetch_all()
    return callback

def wywolaj_z_reki_bez_fetch(zapytanie):
    """Wywolanie zapytania z reki
    """
    DB.execute_command(zapytanie)
    #callback = DB.fetch_all()
    #return callback

def zatwierdz():
    """Commit - tylko dla funkcji bez commit (dodaj_bez_commit, ...)
    """
    DB.make_changes()

def odrzuc():
    """rollback - jesli wywolamy przed commit, wszystkie zmiany zostana usuniete
    """
    DB.discard_changes()
