#from database_connection import Database
import Pyro4
import sys
import os.path
import inspect

#external
sys.path.append(os.path.abspath('../'))
import database_tools.database_main_queries as DB_queries
#from database_tools.database_aggregator import DB_aggregator

from tables.Dostawa import DOSTAWA as Dostawa
from tables.Faktura_Towar_Usluga import FAKTURA_TOWAR_USLUGA as Faktura_Towar_Usluga
from tables.Forma_Platnosci import FORMA_PLATNOSCI as Forma_Platnosci
from tables.Jednostka_Miary import JEDNOSTKA_MIARY as Jednostka_Miary
from tables.Klient_Dzialalnosc_Gospodarcza import KLIENT_DZIALALNOSC_GOSPODARCZA as Klient_Dzialalnosc_Gospodarcza
from tables.Nabywca import NABYWCA as Nabywca
from tables.Osoba_Wystawiajaca_Dokument import OSOBA_WYSTAWIAJACA_DOKUMENT as Osoba_Wystawiajaca_Dokument
from tables.Pkwiu import PKWIU as Pkwiu
from tables.Sprzedawca import SPRZEDAWCA as Sprzedawca
from tables.Towar_Usluga import TOWAR_USLUGA as Towar_Usluga
from tables.Vat import VAT as Vat
from tables.Serwer_Plikowy import SERWER_PLIKOWY as Serwer_Plikowy
from tables.Templatka import TEMPLATKA as Templatka

#from ReportLab.pdf_generator import Faktura_Template
from tables.Template import Template
from add_ons.logger import Logger

sys.path.append(os.path.abspath('../../'))
import config
from ReportLab.pdf_generator import Faktura_Template
from add_ons.logger import Logger
import uuid

@Pyro4.expose
class FAKTURA(Template):
    def __init__(self):
        Template.__init__(
                            self,
                            db_table_name = 'faktura',
                            human_name = 'faktura',
                            db_table_main_key_name='id_faktura'
                          )
        self.logger = Logger()
        self.logger.info(inspect.stack(), 'run')

        self.sciezka_zapisu_pliku = config.serwer_plikowy['sciezka_do_katalogu']

    def pokaz_ostatni_nr_faktury_faktury_po_nip_sprzedawca_prefix_nr_faktury(self, nip, prefix_nr_faktury):
        '''
        nr_faktury/prefix np nr_faktury/mm/rrrr
        '''
        nr_faktury = 0
        query = 'select nr_faktury from faktura \
                inner join sprzedawca on (faktura.id_sprzedawca = sprzedawca.id_sprzedawca) \
                where nip = \'%s\' and prefix_nr_faktury = \'%s\'' % (str(nip), str(prefix_nr_faktury))

        callback = DB_queries.wywolaj_z_reki(query)

        if len(callback) > 0:
            nr_faktury = callback[0]['nr_faktury']

        return int(nr_faktury)


    def __generuj_nazwe_pliku(self, sciezka_zapisu_pliku):
        import socket

        ip_host = socket.gethostbyname(socket.gethostname())
        nazwa_pliku = ('%s.pdf') % str(uuid.uuid4()).replace('-', '')
        nazwa_pliku_z_sciezka = ('%s%s') % (sciezka_zapisu_pliku, nazwa_pliku)

        return nazwa_pliku_z_sciezka, nazwa_pliku, ip_host

    def __dodaj_fakture_do_bazy(self, ip, nazwa_pliku, towar_usluga, sprzedawca, nabywca, forma_platnosci, osoba_wystawiajaca_dokument, dostawa, faktura, nr_faktury_sprawdz = True):
        """
        nr_faktury_sprawdz - jesli nie chcemy zeby sprawdzal system nr_faktury tylko wpisal ta, ktora poda klient
        """
        # sprzedawca
        self.Sprzedawca = Sprzedawca()
        id_sprzedawca = self.Sprzedawca.dodaj_bez_commit_jesli_unikalna_tabela(self.Sprzedawca.nazwa_tabeli(), sprzedawca)['id_sprzedawca']
        nip_sprzedawca = self.Sprzedawca.pokaz(id_sprzedawca)[0]['nip']

        # nabywca
        self.Nabywca = Nabywca()
        id_nabywca = self.Nabywca.dodaj_bez_commit_jesli_unikalna_tabela(self.Nabywca.nazwa_tabeli(), nabywca)['id_nabywca']

        # dorma platnosci
        self.Forma_Platnosci = Forma_Platnosci()
        id_forma_platnosci = self.Forma_Platnosci.dodaj_bez_commit_jesli_unikalna_tabela(self.Forma_Platnosci.nazwa_tabeli(), forma_platnosci)['id_forma_platnosci']

        # osoba wystawiajaca
        self.Osoba_Wystawiajaca_Dokument = Osoba_Wystawiajaca_Dokument()
        id_osoba_wystawiajaca_dokument = self.Osoba_Wystawiajaca_Dokument.dodaj_bez_commit_jesli_unikalna_tabela(
                                                self.Osoba_Wystawiajaca_Dokument.nazwa_tabeli(), osoba_wystawiajaca_dokument)['id_osoba_wystawiajaca_dokument']

        # dostawa
        self.Dostawa = Dostawa()
        id_dostawa = self.Dostawa.dodaj_bez_commit_jesli_unikalna_tabela(self.Dostawa.nazwa_tabeli(), dostawa)['id_dostawa']

        # Templatka
        self.Templatka = Templatka()
        id_templatka = self.Templatka.dodaj_bez_commit_jesli_unikalna_tabela(self.Templatka.nazwa_tabeli(), {'nazwa':'stolargo_template_1'})['id_templatka']

        # serwer plikowy
        self.Serwer_Plikowy = Serwer_Plikowy()
        id_serwer_plikowy = self.Serwer_Plikowy.dodaj_bez_commit_jesli_unikalna_tabela(self.Serwer_Plikowy.nazwa_tabeli(), {'ip':ip, 'sciezka_do_plikow':self.sciezka_zapisu_pliku})['id_serwer_plikowy']

        # faktura

        # nr = +1
        if nr_faktury_sprawdz:
            nr_faktury = self.pokaz_ostatni_nr_faktury_faktury_po_nip_sprzedawca_prefix_nr_faktury(nip_sprzedawca, faktura['prefix_nr_faktury'])
            faktura['nr_faktury'] = nr_faktury + 1

        faktura['id_sprzedawca'] = id_sprzedawca
        faktura['id_nabywca'] = id_nabywca
        faktura['id_forma_platnosci'] = id_forma_platnosci
        faktura['id_osoba_wystawiajaca_dokument'] = id_osoba_wystawiajaca_dokument
        faktura['id_dostawa'] = id_dostawa
        faktura['nazwa_pliku'] = nazwa_pliku
        faktura['id_templatka'] = id_templatka
        faktura['id_serwer_plikowy'] = id_serwer_plikowy

        self.id_faktura = self.dodaj_bez_commit(faktura)['id_faktura']

        # towar_usluga
        self.Vat = Vat()
        self.Towar_Usluga = Towar_Usluga()
        self.Jednostka_Miary = Jednostka_Miary()
        self.Faktura_Towar_Usluga = Faktura_Towar_Usluga()

        for element in towar_usluga:
            # jesli istnieje zwraca id, jesli nie istnieje dodaje nowe i zwraca id nowego
            id_vat = self.Vat.dodaj_bez_commit_jesli_unikalna_tabela(self.Vat.nazwa_tabeli(), {'wartosc':element['vat']})['id_vat']
            id_towar_usluga = self.Towar_Usluga.dodaj_bez_commit_jesli_unikalna_tabela(self.Towar_Usluga.nazwa_tabeli(),
                {
                 'nazwa':element['nazwa'], 'cena_netto':element['cena_netto'], 'wartosc_netto':element['wartosc_netto'],
                 'cena_brutto': element['cena_brutto'], 'wartosc_brutto':element['wartosc_brutto'], 'id_vat':id_vat
                })['id_towar_usluga']

            # dodaje wiele do wiele-do-wielu
            # jesli istnieje zwraca id, jesli nie istnieje dodaje nowe i zwraca id nowego
            id_jednostka_miary = self.Jednostka_Miary.dodaj_bez_commit_jesli_unikalna_tabela(self.Jednostka_Miary.nazwa_tabeli(), {'nazwa':element['jednostka_miary']})['id_jednostka_miary']
            DB_queries.dodaj_bez_commit_bez_zwrotki(self.Faktura_Towar_Usluga.nazwa_tabeli(), {'id_faktura':self.id_faktura, 'id_towar_usluga':id_towar_usluga, 'id_jednostka_miary':id_jednostka_miary, 'ilosc':element['ilosc']})

    def dodaj_stolargo_template_1(self, towar_usluga, sprzedawca, nabywca, forma_platnosci, osoba_wystawiajaca_dokument, dostawa, faktura):
        """
        funkcja generuje plik pdf i dodaje dane do bazy

        przykladowa zwrotka:
        {'ip_host': '127.0.1.1', 'nazwa_pliku': '38a3827d30e64988a6f64f98997b1573.pdf', 'nazwa_pliku_sciezka': '/home/mateusz/Temp/temp/38a3827d30e64988a6f64f98997b1573.pdf', 'id_faktura': 10}

        przykład na wejściu:
        towar_usluga =  [
                            {'nazwa':'Usługi Informatyczne', 'ilosc': 1, 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':122200.12, 'vat':'23', 'cena_brutto':123.23, 'wartosc_brutto':'9090'},
                            {'nazwa':'dupa', 'ilosc': 1, 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2', 'cena_brutto':123.23, 'wartosc_brutto':'9090'},
                            {'nazwa':'dupa', 'ilosc': 1, 'jednostka_miary':'szt.', 'cena_netto':100.00, 'wartosc_netto':100.12, 'vat':'2', 'cena_brutto':123.23, 'wartosc_brutto':'9090'},
                        ]

        sprzedawca = { 'nazwa_firmy':'Stolargo', 'adres_ulica':'Jagiellońska 74', 'adres_miejscowosc':'33-300 Nowy Sącz', 'nip':'734564874', 'bank_nazwa':'ING Bank Śląski', 'bank_nr_konta':'CCAAAAAAAABBBBBBBBBBBBBBBB'}
        nabywca = { 'nazwa_firmy':'e-grupa', 'adres_ulica':'Jamnicka 174', 'adres_miejscowosc':'33-444 Kraków', 'nip':'123123123', 'bank_nazwa':'BGŻ', 'bank_nr_konta':'123123123666666666666'}
        forma_platnosci = {'nazwa':'przelew'}
        osoba_wystawiajaca_dokument = {'imie_nazwisko':'Mateusz Iwański'}
        dostawa = {'nazwa':'odbiór osobisty'}
        faktura = {
                    'nr_faktury':'FV234/56/67', 'nr_zamowienia':'123456', 'miejsce_wystawienia':'Nowy Sącz', 'data_wystawienia':'14/07/2017', 'termin_platnosc':'25/03/2017',
                    'oryginal':True, 'kopia':False, 'duplikat':False,
                    'podsumowanie_zaplacono':'0', 'podsumowanie_do_zaplaty':'1234',
                    'podsumowanie_produktow_uslug_ilosc':'56', 'podsumowanie_produktow_uslug_cena_netto':'345.78', 'podsumowanie_produktow_uslug_cena_brutto':'500.45',
                    'podsumowanie_produktow_uslug_wartosc_netto':'2345', 'podsumowanie_produktow_uslug_wartosc_brutto':'456', 'uwagi':'jakieś tam uwagi'
                  }

        suma_wartosc_netto = suma_cena_netto * suma_ilość
        """
        ######
        # funkcje wewnetrzne
        ######
        def generuj_pdf(self, sciezka_zapisu_pliku, towar_usluga, sprzedawca, nabywca, forma_platnosci, osoba_wystawiajaca_dokument, dostawa, faktura):
            nazwa_pliku_z_sciezka, nazwa_pliku, ip_host = self.__generuj_nazwe_pliku(sciezka_zapisu_pliku)
            self.faktura = Faktura_Template()
            self.faktura.stolargo_template_1(
                                                nazwa_pliku=nazwa_pliku_z_sciezka, miejsce_wystawienia=faktura['miejsce_wystawienia'], data_wystawienia=faktura['data_wystawienia'],
                                                nr_faktury=faktura['nr_faktury'], prefix_nr_faktury=faktura['prefix_nr_faktury'], oryginal=faktura['oryginal'],
                                                nabywca_nazwa_firmy=nabywca['nazwa_firmy'], nabywca_adres_ulica=nabywca['adres_ulica'],
                                                nabywca_adres_miejscowosc=nabywca['adres_miejscowosc'], nabywca_nip=nabywca['nip'],
                                                sprzedawca_nazwa_firmy=sprzedawca['nazwa_firmy'], sprzedawca_adres_ulica=sprzedawca['adres_ulica'],
                                                sprzedawca_adres_miejscowosc=sprzedawca['adres_miejscowosc'], sprzedawca_nip=sprzedawca['nip'], sprzedawca_bank_nazwa=sprzedawca['bank_nazwa'],
                                                sprzedawca_konto_bankowe=sprzedawca['bank_nr_konta'],
                                                lista_produktow_uslug=towar_usluga,
                                                podsumowanie_produktow={
                                                                        'suma_ilosc':faktura['podsumowanie_produktow_uslug_ilosc'],
                                                                        'suma_cena_netto':faktura['podsumowanie_produktow_uslug_cena_netto'],
                                                                        'suma_wartosc_netto':faktura['podsumowanie_produktow_uslug_wartosc_netto'],
                                                                        'suma_wartosc_brutto':faktura['podsumowanie_produktow_uslug_wartosc_brutto']
                                                                        },
                                                forma_platnosci=forma_platnosci['nazwa'], termin_platnosci=faktura['termin_platnosc'],
                                                dostawa=dostawa['nazwa'], zaplacono=faktura['podsumowanie_zaplacono'], do_zaplaty=faktura['podsumowanie_do_zaplaty'],
                                                osoba_wystawiajaca_dokument=osoba_wystawiajaca_dokument['imie_nazwisko'],
                                                nr_zamowienia=faktura['nr_zamowienia']
                                             )
            return nazwa_pliku_z_sciezka, nazwa_pliku, ip_host
        #####
        # koniec funkcji wewnetrznych
        #####
        print('1')
        # generuje plik pdf
        nazwa_sciezka_pliku, nazwa_pliku, ip_host = generuj_pdf(
                                                                    self,
                                                                    sciezka_zapisu_pliku=self.sciezka_zapisu_pliku,
                                                                    towar_usluga=towar_usluga, sprzedawca=sprzedawca, nabywca=nabywca, forma_platnosci=forma_platnosci,
                                                                    osoba_wystawiajaca_dokument=osoba_wystawiajaca_dokument, dostawa=dostawa, faktura=faktura
                                                                )
        print('2')
        self.__dodaj_fakture_do_bazy(
                                        ip=ip_host,
                                        nazwa_pliku=nazwa_pliku,
                                        towar_usluga=towar_usluga, sprzedawca=sprzedawca, nabywca=nabywca, forma_platnosci=forma_platnosci,
                                        osoba_wystawiajaca_dokument=osoba_wystawiajaca_dokument, dostawa=dostawa, faktura=faktura
                                     )
        self.zatwierdz()  # zatwierdzam zmiany w bazie

        return {'id_faktura':self.id_faktura, 'nazwa_pliku_sciezka':nazwa_sciezka_pliku, 'nazwa_pliku':nazwa_pliku, 'ip_host':ip_host}
