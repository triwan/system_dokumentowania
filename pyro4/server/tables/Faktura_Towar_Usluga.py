#from database_connection import Database
import Pyro4
import sys
import os.path
import inspect

#external
sys.path.append(os.path.abspath('../'))
import database_tools.database_main_queries as DB_queries
#from database_tools.database_aggregator import DB_aggregator
from tables.Template import Template
from add_ons.logger import Logger

# wiele-do-wielu
@Pyro4.expose
class FAKTURA_TOWAR_USLUGA(Template):
    def __init__(self):
        Template.__init__(
                            self,
                            db_table_name = 'faktura_towar_usluga',
                            human_name = 'towar/usluga polaczona z faktura (wiele-do-wielu)',
                            db_table_main_key_name=''
                          )
        self.logger = Logger()
        self.logger.info(inspect.stack(), 'run')

