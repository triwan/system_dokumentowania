#from database_connection import Database
import Pyro4
import sys
import os.path
import inspect

#external
sys.path.append(os.path.abspath('../'))
import database_tools.database_main_queries as DB_queries
#from database_tools.database_aggregator import DB_aggregator
from tables.Template import Template
from add_ons.logger import Logger

# wiele-do-wielu
@Pyro4.expose
class KLIENT_DZIALALNOSC_GOSPODARCZA(Template):
    def __init__(self):
        Template.__init__(
                            self,
                            db_table_name = 'klient_dzialalnosc_gospodarcza',
                            human_name = 'klient polaczony z dzialalnoscia (wiele-do-wielu)',
                            db_table_main_key_name=''
                          )
        self.logger = Logger()
        self.logger.info(inspect.stack(), 'run')

    def pokaz(self, id_klient):
        """Wyszukuje obiekt po id_klient
        nie posiada wlasnego id, dlatego nadpisana
        """
        callback = DB_queries.wywolaj_z_reki(('SELECT * FROM klient_dzialalnosc_gospodarcza WHERE id_klient = %s' % (id_klient)))
        return callback

    def dodaj_bez_commit(self, dane):
        '''
        jesli wiele
        dane - { 'id_dzialalnosc_gospodarcza' : [wartosc1, wartosc2, ...], 'id_klient' : wartosc }
        jesli 1
        dane - { 'id_dzialalnosc_gospodarcza' : wartosc, 'id_klient' : wartosc }
        '''
        if type(dane['id_dzialalnosc_gospodarcza']) is not list:
            DB_queries.dodaj_bez_commit_bez_zwrotki(self.db_table_name, dane)
        else: # wiecej elementow
            for id in dane['id_dzialalnosc_gospodarcza']:
                DB_queries.dodaj_bez_commit_bez_zwrotki(self.db_table_name,
                           {'id_dzialalnosc_gospodarcza':id, 'id_klient' : dane['id_klient']})

    def usun_stare_powiazania_i_dodaj_nowe_bez_commit(self, data):
        """Usuwa element z bazy i dodaje nowe
        usuwa KLIENT_DZIALALNOSC_GOSPODARCZA gdzie wystepuje id_klient,
        nastepnie dodaje nowe
        funkcja przydatna w relcji wiele-do-wielu gdzie tabela nie ma wlasnego id a chemy
        w tej tabeli coś modyfikować
        data = {id_dzialalnosc_gospodarcza:..., id_klient:..}
        """
        query = 'DELETE FROM klient_dzialalnosc_gospodarcza WHERE id_klient = %s' % (data['id_klient'])
        DB_queries.DB.execute_command(query)
        self.dodaj_bez_commit(data)
