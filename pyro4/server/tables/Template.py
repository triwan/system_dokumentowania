#from database_connection import Database
import Pyro4
import sys
import os.path
import inspect

#external
sys.path.append(os.path.abspath('../'))
import database_tools.database_main_queries as DB_queries
#from database_tools.database_aggregator import DB_aggregator
from add_ons.logger import Logger

@Pyro4.expose
class Template(object):
    def __init__(self, db_table_name, human_name, db_table_main_key_name):
        self.db_table_name = db_table_name
        self.human_name = human_name
        self.db_table_main_key_name = db_table_main_key_name

    def nazwa_tabeli(self):
        """Zwraca nazwe tabeli
        """
        return self.db_table_name

    def ludzka_nazwa(self):
        """Zwraca "ludzka" nazwe
        """
        return self.human_name

    def pokaz_nazwe_klucza_podstawowego(self):
        """Zwraca nazwe kluca podstawowego
        """
        return self.db_table_main_key_name

    def pokaz_ostanie_id(self):
        """Zwraca ostanie id tabeli
        zwraca {'max':...}
        """
        callback = DB_queries.pokaz_ostanie_id(self.db_table_name)
        return callback

    def pokaz(self, id):
        """Odczytaj jeden obiekt z bazy
        """
        callback = DB_queries.pokaz(self.db_table_name, id)
        return callback

    def pokaz_wszystkie(self):
        """Odczytaj wszystkie obiekty z bazy
        """
        callback = DB_queries.pokaz_wszystkie(self.db_table_name)
        return callback

    def usun(self, id):
        """Usuwa element z bazy
        """
        DB_queries.usun(self.db_table_name, id)

    def usun_bez_commit(self, id):
        """Usuwa element z bazy
        """
        DB_queries.usun_bez_commit(self.db_table_name, id)

    def dodaj(self, dane):
        """Dodaje element do bazy
        """
        callback = DB_queries.dodaj(self.db_table_name, dane)
        return callback

    def dodaj_bez_zwrotki(self, dane):
        """Dodaje element do bazy
        nie zwraca id, przydatne do tabel wiele-do-wielu
        """
        callback = DB_queries.dodaj_bez_zwrotki(self.db_table_name, dane)
        return callback

    def dodaj_bez_commit_bez_zwrotki(self, dane):
        """Dodaje element do bazy bez zatwierdzenia
        nie zwraca id, przydatne do tabel wiele-do-wielu
        """
        callback = DB_queries.dodaj_bez_commit_bez_zwrotki(self.db_table_name, dane)
        return callback

    def dodaj_bez_commit(self, dane):
        """Dodaje element do bazy
        po wywolaniu funkcji nalezy wywolac zatwierdz(commit)
        """
        callback = DB_queries.dodaj_bez_commit(self.db_table_name, dane)
        return callback

    def modyfikuj(self, dane):
        """Modyfikuje tabele w bazie
        """
        callback = DB_queries.modyfikuj(self.db_table_name, dane)
        return callback

    def modyfikuj_bez_commit(self, dane):
        """Modyfikuje tabele w bazie
        po wywolaniu funkcji nalezy wywolac zatwierdz(commit)
        """
        callback = DB_queries.modyfikuj_bez_commit(self.db_table_name, dane)
        return callback

    def modyfikuj_wiele(self, dane):
        """Modyfikuje wiele tabel na raz w bazie
        """
        callback = DB_queries.modyfikuj_wiele(self.db_table_name, dane, self.db_table_main_key_name)
        return callback

    def dodaj_wiele(self, dane):
        """Dodaje wiele tabel na raz w bazie
        """
        callback = DB_queries.dodaj_wiele(self.db_table_name, dane, self.db_table_main_key_name)
        return callback


    def szukaj(self, data_tuple):
        """Szukaj tabele w bazie
        """
        callback = DB_queries.szukaj(self.db_table_name, data_tuple)
        return callback

    def sprawdz_czy_unikalna_wartosc(self, db_columne_name, dane):
        """Sprawdz czy dana wartosc wystepuje w tabeli
        """
        callback = DB_queries.sprawdz_czy_unikalna_wartosc(self.db_table_name, db_columne_name, dane)
        return callback

    def sprawdz_czy_unikalna_wartosc_przy_modyfikowaniu(self, db_columne_name, dane, id_tabeli_do_modyfikowania):
        """Sprawdz czy dana wartosc wystepuje w tabeli, ktora modyfikujemy
        nie bierze pod uwage wystepowaniu unikalnosci w tabeli, ktora modyfikujemy
        """
        callback = DB_queries.sprawdz_czy_unikalna_wartosc_przy_modyfikowaniu(
                            self.db_table_name, db_columne_name, dane, id_tabeli_do_modyfikowania)
        return callback

    def dodaj_bez_commit_jesli_unikalna_tabela(self, db_table_name, dane):
        """Dodaje tabele jesli nie istnieje
        zwraca id_tabeli z podanymi danymi
        po wywolaniu funkcji nalezy wywolac commit do zatwierdzenia
        funkcja zwraca - {'id_tabela': wartość}
        """
        callback = DB_queries.dodaj_bez_commit_jesli_unikalna_tabela(db_table_name, dane)
        return callback

    def zatwierdz(self):
        """Commit - tylko dla funkcji bez commit (dodaj_bez_commit, ...)
        """
        callback = DB_queries.zatwierdz()
        return callback

    def odrzuc(self):
        """rollback - jesli wywolamy przed commit, wszystkie zmiany zostana usuniete
        """
        callback = DB_queries.odrzuc()
        return callback
