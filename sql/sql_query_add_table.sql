CREATE TABLE jednostka_miary(
  id_jednostka_miary serial PRIMARY KEY,
  nazwa VARCHAR(30)
);

CREATE TABLE pkwiu(
  id_pkwiu serial PRIMARY KEY,
  symbol VARCHAR(20),
  nazwa TEXT
);

CREATE TABLE vat(
  id_vat serial primary key,
  wartosc smallint
);

CREATE TABLE towar_usluga(
  id_towar_usluga serial PRIMARY KEY,
  id_pkwiu integer,
  id_vat integer not null,
  nazwa VARCHAR(254) NOT NULL,
  cena_netto real Check (cena_netto::numeric = trunc(cena_netto::numeric, 2)) NOT NULL,
  wartosc_netto real Check (cena_netto::numeric = trunc(cena_netto::numeric, 2)) NOT NULL,
  cena_brutto real Check (cena_brutto::numeric = trunc(cena_brutto::numeric, 2)) NOT NULL,
  wartosc_brutto real Check (cena_brutto::numeric = trunc(cena_brutto::numeric, 2)) NOT NULL,

  CONSTRAINT towar_usluga_id_pkwiu_fkey FOREIGN KEY (id_pkwiu)
        REFERENCES pkwiu (id_pkwiu) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT towar_usluga_id_vat_fkey FOREIGN KEY (id_vat)
        REFERENCES vat (id_vat) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION

);

CREATE TABLE nabywca(
  id_nabywca serial PRIMARY key,
  nazwa_firmy varchar(254),
  adres_ulica varchar(254),
  adres_miejscowosc varchar(254),
  bank_nazwa varchar(100),
  bank_nr_konta varchar(26),
  nip varchar(10)
);

CREATE TABLE sprzedawca(
  id_sprzedawca serial PRIMARY key,
  nazwa_firmy varchar(254),
  adres_ulica varchar(254),
  adres_miejscowosc varchar(254),
  bank_nazwa varchar(100),
  bank_nr_konta varchar(26),
  nip varchar(10)
);

create table forma_platnosci(
  id_forma_platnosci serial PRIMARY KEY,
  nazwa varchar(50)
);

create table osoba_wystawiajaca_dokument(
  id_osoba_wystawiajaca_dokument serial PRIMARY KEY,
  imie_nazwisko varchar(254),
  pseudonim varchar(254)
);

create table dostawa(
  id_dostawa serial PRIMARY key,
  nazwa varchar(254)
);

create table templatka(
  id_templatka serial PRIMARY KEY,
  nazwa varchar(254)
);

create table serwer_plikowy(
  id_serwer_plikowy serial PRIMARY KEY,
  ip inet,
  sciezka_do_plikow varchar(254)
);

CREATE TABLE faktura(
  id_faktura serial PRIMARY KEY,
  id_sprzedawca INTEGER not null,
  id_nabywca INTEGER not null,
  id_forma_platnosci INTEGER not null,
  id_osoba_wystawiajaca_dokument INTEGER not null,
  id_dostawa INTEGER not null,
  id_templatka INTEGER not null,
  id_serwer_plikowy INTEGER not null,

  nr_faktury varchar(254) not null,
  prefix_nr_faktury varchar (254),  -- nr_faktury/prefix np nr_faktury/mm/rrrr
  nr_zamowienia varchar(254),

  miejsce_wystawienia varchar(254) not null,
  data_wystawienia date not null,
  termin_platnosc date not null,

  oryginal boolean,
  kopia boolean,
  duplikat boolean,

  podsumowanie_zaplacono real Check (podsumowanie_zaplacono::numeric = trunc(podsumowanie_zaplacono::numeric, 2)),
  podsumowanie_do_zaplaty real Check (podsumowanie_do_zaplaty::numeric = trunc(podsumowanie_do_zaplaty::numeric, 2)),

  podsumowanie_produktow_uslug_ilosc smallint,
  podsumowanie_produktow_uslug_cena_netto real Check (podsumowanie_produktow_uslug_cena_netto::numeric = trunc(podsumowanie_produktow_uslug_cena_netto::numeric, 2)),
  podsumowanie_produktow_uslug_cena_brutto real Check (podsumowanie_produktow_uslug_cena_brutto::numeric = trunc(podsumowanie_produktow_uslug_cena_brutto::numeric, 2)),
  podsumowanie_produktow_uslug_wartosc_netto real Check (podsumowanie_produktow_uslug_wartosc_netto::numeric = trunc(podsumowanie_produktow_uslug_wartosc_netto::numeric, 2)),
  podsumowanie_produktow_uslug_wartosc_brutto real Check (podsumowanie_produktow_uslug_wartosc_brutto::numeric = trunc(podsumowanie_produktow_uslug_wartosc_brutto::numeric, 2)),

  nazwa_pliku varchar(50) not null,

  uwagi text,
  data_utworzenia timestamp default current_timestamp,

  CONSTRAINT faktura_id_sprzedawca_fkey FOREIGN KEY (id_sprzedawca)
        REFERENCES sprzedawca (id_sprzedawca) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_nabywca_fkey FOREIGN KEY (id_nabywca)
        REFERENCES nabywca (id_nabywca) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_forma_platnosci_fkey FOREIGN KEY (id_forma_platnosci)
        REFERENCES forma_platnosci (id_forma_platnosci) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_osoba_wystawiajaca_dokument_fkey FOREIGN KEY (id_osoba_wystawiajaca_dokument)
        REFERENCES osoba_wystawiajaca_dokument (id_osoba_wystawiajaca_dokument) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_dostawa_fkey FOREIGN KEY (id_dostawa)
        REFERENCES dostawa (id_dostawa) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_templatka_fkey FOREIGN KEY (id_templatka)
        REFERENCES templatka (id_templatka) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_id_serwer_plikowy_fkey FOREIGN KEY (id_serwer_plikowy)
        REFERENCES serwer_plikowy (id_serwer_plikowy) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);


--wiele-do-wielu
create table faktura_towar_usluga(
  id_faktura INTEGER NOT NULL,
  id_towar_usluga INTEGER NOT NULL,
  id_jednostka_miary INTEGER NOT NULL,
  ilosc real Check (ilosc::numeric = trunc(ilosc::numeric, 2)),

  CONSTRAINT faktura_towar_usluga_id_faktura_fkey FOREIGN KEY (id_faktura)
        REFERENCES faktura (id_faktura) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_towar_usluga_id_towar_usluga_fkey FOREIGN KEY (id_towar_usluga)
        REFERENCES towar_usluga (id_towar_usluga) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT faktura_towar_usluga_id_jednostka_miary_fkey FOREIGN KEY (id_jednostka_miary)
        REFERENCES jednostka_miary (id_jednostka_miary) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);
